﻿using CommandLine;
using CommandLine.Text;

namespace OtdsClient
{
    /// <summary>
    /// Options class for command line options
    /// </summary>
    public class Options
    {
        /// <summary>
        /// CWS host url
        /// </summary>
        [Option('h', "host", Required = true, HelpText = "CWS host url e.g. localhost")]
        public string HostUrl { get; set; }

        /// <summary>
        /// User login name
        /// </summary>
        [Option('u', "user", Required = true, HelpText = "User login name")]
        public string User { get; set; }

        /// <summary>
        /// User's password
        /// </summary>
        [Option('p', "password", Required = true, HelpText = "User's password")]
        public string Password { get; set; }

        /// <summary>
        /// Parser state
        /// </summary>
        [ParserState]
        public IParserState LastParserState { get; set; }

        /// <summary>
        /// Output for usage
        /// </summary>
        /// <returns></returns>
        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
              (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }
}