﻿using System;
using System.ServiceModel;

namespace OtdsClient
{
    class Program
    {
        /// <summary>
        /// Main entry point of program
        /// </summary>
        /// <param name="args">string array of commandline parameters</param>
        static void Main(string[] args)
        {
            try
            {
                // create the options structure to parse command line options
                var __o = new Options();
                if (CommandLine.Parser.Default.ParseArguments(args, __o))
                {
                    // all options are present, so use them
                    Authenticate(__o.HostUrl, __o.User, __o.Password);
                }
                else
                {
                    // Display the default usage information
                    Console.WriteLine(__o.GetUsage());
                }
            }
            catch (Exception)
            {
                // do nothing
            }

            // pause and quit
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        /// <summary>
        /// Authenticates a user against an OTDS/OTCS environment
        /// </summary>
        /// <param name="cwsHost">the url of the host</param>
        /// <param name="user">the user name</param>
        /// <param name="password">the password for the user</param>
        static void Authenticate(string cwsHost, string user, string password)
        {
            try
            {
                // inform the user
                Console.WriteLine($"Attempting to authenticate user {user} against OTCS server {cwsHost}");

                // create clients
                var __otds = new OtdsAuth.AuthenticationClient();
                var __otcs = new OtcsAuth.AuthenticationClient();

                // first go to the otcs server
                __otcs.Endpoint.Address = new EndpointAddress($"http://{cwsHost}/cws/Authentication.svc");

                // get the otds server (a new and nice function)
                var __otdsServer = __otcs.GetOTDSServer();
                if (string.IsNullOrEmpty(__otdsServer))
                {
                    // tell the user what's going on
                    Console.WriteLine($"{cwsHost} does not use OTDS");

                    // use that token to authenticate to this server
                    var __otcsraw = __otcs.AuthenticateUser(user, password);
                    if (__otcsraw.Length == 0)
                    {
                        throw new Exception($"Could not authenticate {user} on {cwsHost}");
                    }
                    Console.WriteLine($"Authenticated {user} on {cwsHost}");
                }
                else
                {
                    // tell the user what's going on
                    Console.WriteLine($"{cwsHost} uses the OTDS server at {__otdsServer}, getting ticket...");

                    // set the endpoint
                    __otds.Endpoint.Address = new EndpointAddress($"{__otdsServer}/otdsws/services/Authentication");

                    // authenticate on the otds server
                    var __otdstoken = new OtdsAuth.OTAuthentication();
                    var __otdsraw = __otds.Authenticate(ref __otdstoken, user, password);
                    if (string.IsNullOrEmpty(__otdsraw))
                    {
                        throw new Exception($"Could not authenticate {user} on {__otdsServer}");
                    }
                    Console.WriteLine($"Acquired ticket for {user} from {__otdsServer}");

                    // use that token to authenticate to the other server
                    var __otcsraw = __otcs.ValidateUser(__otdsraw);
                    if (string.IsNullOrEmpty(__otcsraw))
                    {
                        throw new Exception($"Could not validate the ticket from {__otdsServer} on {cwsHost}");
                    }
                    Console.WriteLine($"Authenticated {user} on {cwsHost}");
                }

            }
            catch (Exception __e)
            {
                Console.WriteLine(__e);
            }
        }
    }
}
