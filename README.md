# OtdsClient

## Description

This quick tool provide an example of how to authenticate to an OTCS environment, even if it uses OTDS.

## Environment

This project is a Visual Studio 2015 C# project, and it uses the CommandLineParser nuget package.

## To Use

Download the code, build, and enter the command line options required. It will inform you if it authenticated or not.